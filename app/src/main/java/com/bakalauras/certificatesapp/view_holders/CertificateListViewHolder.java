package com.bakalauras.certificatesapp.view_holders;

import android.view.View;
import android.widget.TextView;

import com.bakalauras.certificatesapp.R;

public class CertificateListViewHolder {
    TextView certificateName;
    TextView certificateLastAccessed;

    public CertificateListViewHolder(View view) {
        certificateName = view.findViewById(R.id.certificate_name_label);
        certificateLastAccessed = view.findViewById(R.id.certificate_last_access);
    }

    public void setCertificateName(String certificateName) {
        this.certificateName.setText(certificateName);
    }

    public void setCertificateLastAccessed(String certificateLastAccessed) {
        this.certificateLastAccessed.setText(String.format("Last accessed: %s", certificateLastAccessed));
    }
}
