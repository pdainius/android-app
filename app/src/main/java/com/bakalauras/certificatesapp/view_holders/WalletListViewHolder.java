package com.bakalauras.certificatesapp.view_holders;

import android.view.View;
import android.widget.TextView;

import com.bakalauras.certificatesapp.R;

public class WalletListViewHolder {
    TextView walletName;
    TextView walletLastAccessed;

    public WalletListViewHolder(View view) {
        walletName = view.findViewById(R.id.wallet_name_label);
        walletLastAccessed = view.findViewById(R.id.wallet_last_access);
    }

    public void setWalletName(String walletName) {
        this.walletName.setText(walletName);
    }

    public void setWalletLastAccessed(String walletLastAccessed) {
        this.walletLastAccessed.setText(String.format("Last accessed: %s", walletLastAccessed));
    }
}
