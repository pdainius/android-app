package com.bakalauras.certificatesapp.services;

import android.app.Application;
import android.hardware.biometrics.BiometricManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeyProtection;
import android.util.Log;

import com.bakalauras.certificatesapp.BuildConfig;
import com.bakalauras.certificatesapp.Constants;
import com.bakalauras.certificatesapp.repositories.WalletRepository;

import org.spongycastle.asn1.x500.RDN;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.asn1.x500.style.IETFUtils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;


public class KeyStoreService {
    private static KeyStoreService instance;

    private KeyStoreService(){ }

    public static KeyStoreService getInstance() {
        if (instance == null) {
            instance = new KeyStoreService();
        }

        return instance;
    }

    public void deleteAlias(String alias) {
        try {
            KeyStore keyStore = KeyStore.getInstance(Constants.KEYSTORE_PROVIDER);
            keyStore.load(null);
            keyStore.deleteEntry(alias);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCertificate(String alias) {
        try {
            String certAlias = String.format("%s-%s", Constants.KEYSTORE_CERTIFICATE_ALIAS_PREFIX, alias);
            KeyStore keyStore = KeyStore.getInstance(Constants.KEYSTORE_PROVIDER);
            keyStore.load(null);
            keyStore.deleteEntry(certAlias);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public X509Certificate getCertificate(String alias) {
        try {
            String certAlias = String.format("%s-%s", Constants.KEYSTORE_CERTIFICATE_ALIAS_PREFIX, alias);
            KeyStore keyStore = KeyStore.getInstance(Constants.KEYSTORE_PROVIDER);
            keyStore.load(null);
            return (X509Certificate) keyStore.getCertificate(certAlias);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void storeCertificate(String crtPem, String commonName) {
        try{
            String certAlias = String.format("%s-%s", Constants.KEYSTORE_CERTIFICATE_ALIAS_PREFIX, commonName);
            // Convert crt pem format to a X509Certificate type
            KeyStore keyStore = KeyStore.getInstance(Constants.KEYSTORE_PROVIDER);
            keyStore.load(null);
            InputStream targetStream = new ByteArrayInputStream(crtPem.getBytes());
            X509Certificate cert = (X509Certificate) CertificateFactory
                    .getInstance("X509")
                    .generateCertificate(targetStream);

            // Store certificate in keystore
            keyStore.setCertificateEntry(certAlias, cert);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public KeyPair getKeyPair(String alias) {
        try {
            KeyStore keyStore = KeyStore.getInstance(Constants.KEYSTORE_PROVIDER);
            // Before the keystore can be accessed, it must be loaded.
            keyStore.load(null);
            KeyStore.Entry entry = keyStore.getEntry(alias, null);
            PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
            PublicKey publicKey = keyStore.getCertificate(alias).getPublicKey();
            return new KeyPair(publicKey, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public KeyPair generateSecretKey(String alias) {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance(Constants.KEYSTORE_KEY_ALGORITHM, Constants.KEYSTORE_PROVIDER);
            KeyGenParameterSpec builder;
            builder = new KeyGenParameterSpec.Builder(
                    alias,
                    KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY)
                    .setAlgorithmParameterSpec(new ECGenParameterSpec(Constants.EC_CURVE))
                    .setDigests(KeyProperties.DIGEST_SHA256)
                    //.setUserAuthenticationRequired(true)
                    //.setInvalidatedByBiometricEnrollment(true)
                    .setUserAuthenticationValidityDurationSeconds(5 * 60)
                    .build();
            generator.initialize(builder);
            return generator.generateKeyPair();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
