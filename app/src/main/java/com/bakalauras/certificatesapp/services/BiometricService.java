package com.bakalauras.certificatesapp.services;

import android.content.Context;
import android.util.Log;

import androidx.biometric.BiometricManager;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;

public class BiometricService {
    private static BiometricService instance;
    private static BiometricManager biometricManager;

    private BiometricService(){ }

    public static BiometricService getInstance(Context context) {
        if (instance == null) {
            instance = new BiometricService();
            biometricManager = BiometricManager.from(context);
        }

        return instance;
    }

    public boolean canAuthenticate() {
        switch (biometricManager.canAuthenticate(BIOMETRIC_STRONG )) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("BIOMETRICS", "App can authenticate using biometrics.");
                return true;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("BIOMETRICS", "No biometric features available on this device.");
                return false;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("BIOMETRICS", "Biometric features are currently unavailable.");
                return false;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                return false;
        }
        return false;
    }
}
