package com.bakalauras.certificatesapp.services;

import org.spongycastle.asn1.ASN1ObjectIdentifier;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.asn1.x500.style.IETFUtils;
import org.spongycastle.cert.jcajce.JcaX509CertificateHolder;
import org.spongycastle.openssl.PEMWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class CRTObject {
    private X509Certificate certificate;
    private X500Name subject;
    private X500Name issuer;

    private final String[] keyUsages = new String[]{
            "Digital Signature",
            "Non Repudiation",
            "Key Encipherment",
            "Data Encipherment",
            "Key Agreement",
            "Key CertSign",
            "cRL Sign",
            "Encipher Only",
            "Decipher Only"
    };

    public CRTObject(X509Certificate certificate) throws CertificateEncodingException {
        this.certificate = certificate;

        JcaX509CertificateHolder holder = new JcaX509CertificateHolder(certificate);
        this.subject = holder.getSubject();
        this.issuer =  holder.getIssuer();
    }

    public String getSubjectString(ASN1ObjectIdentifier identifier) {
        try {
            return IETFUtils.valueToString(subject.getRDNs(identifier)[0].getFirst().getValue());
        } catch (Exception e) {
            return "N/A";
        }
    }

    public String getIssuerString(ASN1ObjectIdentifier identifier) {
        try {
            return IETFUtils.valueToString(issuer.getRDNs(identifier)[0].getFirst().getValue());
        } catch (Exception e) {
            return "N/A";
        }
    }

    public String toPem() {
        StringWriter sw = new StringWriter();
        try (PEMWriter pw = new PEMWriter(sw)) {
            pw.writeObject(certificate);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sw.toString();
    }

    public String getKeyUsageUsages() {
        try {
            List<String> selectedKeyUsage = new ArrayList<String>();
            boolean[] keyUsagesBoolean = certificate.getKeyUsage();
            for (int i = 0; i < keyUsagesBoolean.length; i++) {
                if (keyUsagesBoolean[i]) {
                    selectedKeyUsage.add(keyUsages[i]);
                }
            }

            return String.join("\n", selectedKeyUsage);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
