package com.bakalauras.certificatesapp.services;

import org.spongycastle.asn1.ASN1ObjectIdentifier;
import org.spongycastle.asn1.x509.AlgorithmIdentifier;
import org.spongycastle.operator.ContentSigner;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;

public class JCESigner implements ContentSigner {

    private final String DEFAULT_SIGNATURE_ALGORITHM = "SHA256withECDSA";
    private final AlgorithmIdentifier OID = new AlgorithmIdentifier(new ASN1ObjectIdentifier("1.2.840.10045.4.3.2"));

    private final Signature signature;
    private final ByteArrayOutputStream outputStream;

    public JCESigner(PrivateKey privateKey) {
        try {
            outputStream = new ByteArrayOutputStream();
            signature = Signature.getInstance(DEFAULT_SIGNATURE_ALGORITHM);
            signature.initSign(privateKey);
        } catch (GeneralSecurityException gse) {
            throw new IllegalArgumentException(gse.getMessage());
        }
    }

    @Override
    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return OID;
    }

    @Override
    public OutputStream getOutputStream() {
        return outputStream;
    }

    @Override
    public byte[] getSignature() {
        try {
            signature.update(outputStream.toByteArray());
            return signature.sign();
        } catch (GeneralSecurityException gse) {
            gse.printStackTrace();
            return null;
        }
    }
}
