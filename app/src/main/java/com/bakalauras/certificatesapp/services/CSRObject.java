package com.bakalauras.certificatesapp.services;

import org.spongycastle.asn1.ASN1ObjectIdentifier;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.asn1.x500.style.IETFUtils;

import java.util.HashMap;
import java.util.Map;

public class CSRObject {
    private String commonName;
    private String country;
    private String state;
    private String locality;
    private String organization;
    private String organizationalUnit;

    public CSRObject() {}

    public CSRObject(String commonName, String country, String state, String locality, String organization, String organizationalUnit) {
        this.commonName = commonName;
        this.country = country;
        this.state = state;
        this.locality = locality;
        this.organization = organization;
        this.organizationalUnit = organizationalUnit;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        this.organizationalUnit = organizationalUnit;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getLocality() {
        return locality;
    }

    public String getOrganization() {
        return organization;
    }

    public String getOrganizationalUnit() {
        return organizationalUnit;
    }

    public void fromX500Name(X500Name subject) {
        this.commonName = IETFUtils.valueToString(subject.getRDNs(BCStyle.CN)[0].getFirst().getValue());
        this.country = IETFUtils.valueToString(subject.getRDNs(BCStyle.C)[0].getFirst().getValue());
        this.state = IETFUtils.valueToString(subject.getRDNs(BCStyle.ST)[0].getFirst().getValue());
        this.locality = IETFUtils.valueToString(subject.getRDNs(BCStyle.L)[0].getFirst().getValue());
        this.organization = IETFUtils.valueToString(subject.getRDNs(BCStyle.O)[0].getFirst().getValue());

        try {
            this.organizationalUnit = IETFUtils.valueToString(subject.getRDNs(BCStyle.OU)[0].getFirst().getValue());
        } catch (Exception ignored){}
    }

    @Override
    public String toString() {
        String subjects = String.format("CN=%s", getCommonName());
        Map<String, String> subjectsMap =  new HashMap<String, String>() {{
            put("C", getCountry());
            put("ST", getState());
            put("L", getLocality());
            put("O", getOrganization());
            put("OU", getOrganizationalUnit());
        }};

        for(String key : subjectsMap.keySet()) {
            String value = subjectsMap.get(key);
            if(value != null && !value.equals("")){
                subjects = subjects.concat(String.format(",%s=%s", key, value));
            }
        }
        return subjects;
    }
}
