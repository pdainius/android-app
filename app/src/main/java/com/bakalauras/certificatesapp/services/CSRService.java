package com.bakalauras.certificatesapp.services;


import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x509.BasicConstraints;
import org.spongycastle.asn1.x509.Extension;
import org.spongycastle.asn1.x509.ExtensionsGenerator;
import org.spongycastle.openssl.jcajce.JcaPEMWriter;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.spongycastle.util.io.pem.PemObject;
import org.spongycastle.util.io.pem.PemReader;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.KeyPair;

public class CSRService {
    private static CSRService instance;

    private CSRService() {}

    public static CSRService getInstance() {
        if (instance == null) {
            instance = new CSRService();
        }

        return instance;
    }

    //Create the certificate signing request (CSR) from private and public keys
    public PKCS10CertificationRequest generateCSR(KeyPair keyPair, CSRObject csrSubjects) throws IOException {
        ContentSigner signer = new JCESigner(keyPair.getPrivate());

        PKCS10CertificationRequestBuilder csrBuilder = new JcaPKCS10CertificationRequestBuilder(new X500Name(csrSubjects.toString()), keyPair.getPublic());
        ExtensionsGenerator extensionsGenerator = new ExtensionsGenerator();
        extensionsGenerator.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
        return csrBuilder.build(signer);
    }

    public PKCS10CertificationRequest csrFromPem(String csrPem) throws IOException {
        StringReader reader = new StringReader(csrPem);
        PemReader pemReader = new PemReader(reader);
        PemObject pemObject = pemReader.readPemObject();
        PKCS10CertificationRequest csr = new PKCS10CertificationRequest(pemObject.getContent());

        pemReader.close();
        reader.close();

        return csr;
    }

    public String csrToPem(PKCS10CertificationRequest csr) throws IOException {
        PemObject pemObject = new PemObject("CERTIFICATE REQUEST", csr.getEncoded());
        StringWriter str = new StringWriter();
        JcaPEMWriter pemWriter = new JcaPEMWriter(str);
        pemWriter.writeObject(pemObject);
        pemWriter.close();
        str.close();
        return str.toString();
    }
}
