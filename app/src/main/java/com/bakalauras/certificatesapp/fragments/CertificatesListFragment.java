package com.bakalauras.certificatesapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.activities.CertificateDetailsActivity;
import com.bakalauras.certificatesapp.activities.GenerateCSRActivity;
import com.bakalauras.certificatesapp.activities.MainActivity;
import com.bakalauras.certificatesapp.activities.WalletDataActivity;
import com.bakalauras.certificatesapp.adapters.CertificateListAdapter;
import com.bakalauras.certificatesapp.adapters.WalletListAdapter;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.services.BiometricService;
import com.bakalauras.certificatesapp.view_models.CertificateViewModel;
import com.bakalauras.certificatesapp.view_models.CertificateViewModelFactory;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.spongycastle.asn1.x509.CertificateList;

import java.security.cert.Certificate;
import java.util.List;
import java.util.concurrent.Executor;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;

public class CertificatesListFragment extends Fragment {

    private BiometricService biometricService;

    private FloatingActionButton certificateCreateFab;
    private String selectedWalletAlias;
    private String selectedCertificateName;
    private TextView certificatesCountTextView;
    private ListView certificateListView;

    private BiometricPrompt biometricPrompt;

    private CertificateListAdapter certificateListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        selectedWalletAlias = args.getString("selectedWalletAlias");
        return inflater.inflate(R.layout.fragment_certificates_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        biometricService = BiometricService.getInstance(getContext());

        setActivityViews(view);
        setOnClickListeners();
        setBiometricPrompt();
        loadCertificateList();
    }

    private void setBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(getContext());
        biometricPrompt = new BiometricPrompt(CertificatesListFragment.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(getContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                openCertificateDetailsActivity();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void openCertificateDetailsActivity() {
        Intent intent = new Intent(getActivity(), CertificateDetailsActivity.class);
        intent.putExtra("selectedWalletAlias", selectedWalletAlias);
        intent.putExtra("selectedCertificateName", selectedCertificateName);
        startActivity(intent);
    }

    private void loadCertificateList() {
        try {
            // Get the ViewModel.
            CertificateViewModelFactory factory = new CertificateViewModelFactory(getActivity().getApplication(), selectedWalletAlias);
            CertificateViewModel model = new ViewModelProvider(this, factory).get(CertificateViewModel.class);

            // Create the observer which updates the UI.
            final Observer<List<CertificateEntity>> certificateListObserver = certificates -> {
                certificateListAdapter = new CertificateListAdapter(getContext(), certificates);
                certificateListView.setAdapter(certificateListAdapter);
                setCertificatesCount(certificateListAdapter.getCount());
            };
            model.getAllCertificates().observe(getViewLifecycleOwner(), certificateListObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setActivityViews(View view) {
        certificateCreateFab = view.findViewById(R.id.certificate_create_fab);
        certificateListView = view.findViewById(R.id.certificates_list_view);
        certificatesCountTextView = view.findViewById(R.id.certificates_count_label);
    }

    private void setOnClickListeners() {
        certificateCreateFab.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), GenerateCSRActivity.class);
            intent.putExtra("selectedWalletAlias", selectedWalletAlias);
            startActivity(intent);
        });

        certificateListView.setOnItemClickListener((parent, view, i, l) -> {
            // Prompt appears when user clicks "Log in".
            // Consider integrating with the keystore to unlock cryptographic operations,
            // if needed by your app.
            if(biometricService.canAuthenticate()) {
                selectedCertificateName = certificateListAdapter.getItem(i).getCertificateName();
                BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                        .setTitle("Certificate authentication")
                        .setSubtitle("Use biometric authentication to access selected certificate")
                        .setNegativeButtonText("Close")
                        .setAllowedAuthenticators(BIOMETRIC_STRONG)
                        .setConfirmationRequired(false)
                        .build();
                biometricPrompt.authenticate(promptInfo);
            } else {
                Toast.makeText(getContext(),
                        "Please setup your biometric authentication", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setCertificatesCount(int certificatesCount) {
        switch (certificatesCount){
            case 0:
                certificatesCountTextView.setText("No certificates found");
                break;
            case 1:
                certificatesCountTextView.setText("1 certificate shown");
                break;
            default:
                certificatesCountTextView.setText(String.format("%s certificates shown", certificatesCount));
        }
    }
}
