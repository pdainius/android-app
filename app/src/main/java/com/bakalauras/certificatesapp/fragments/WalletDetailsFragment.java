package com.bakalauras.certificatesapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.WalletRepository;
import com.bakalauras.certificatesapp.services.KeyStoreService;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.w3c.dom.Text;

import java.sql.Date;

public class WalletDetailsFragment extends Fragment {

    private String selectedWalletAlias;
    private TextView walletNotesPreviewTextView;
    private TextView walletCreatedAtTextView;
    private TextView walletLastAccessTextView;
    private TextView walletNameTextView;
    private MaterialButton walletDeleteButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        selectedWalletAlias = args.getString("selectedWalletAlias");
        return inflater.inflate(R.layout.fragment_wallet_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setActivityViews(view);
        setOnClickListeners();

        walletNameTextView.setText(selectedWalletAlias);
        loadWalletData();
    }

    private void loadWalletData() {
        WalletViewModel model = new ViewModelProvider(this).get(WalletViewModel.class);

        final Observer<WalletEntity> walletObserver = wallet -> {
            try {
                walletCreatedAtTextView.setText(wallet.getCreatedAt().toString());
                walletLastAccessTextView.setText(wallet.getAccessedAt().toString());
                walletNotesPreviewTextView.setText((wallet.getNote() == null || wallet.getNote().equals("")) ? "N/A" : wallet.getNote());
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        model.getSelectedWallet(selectedWalletAlias).observe(getActivity(), walletObserver);
    }

    private void setActivityViews(View view) {
        walletCreatedAtTextView = view.findViewById(R.id.wallet_created_at);
        walletDeleteButton = view.findViewById(R.id.wallet_delete_button);
        walletNameTextView = view.findViewById(R.id.wallet_name);
        walletLastAccessTextView = view.findViewById(R.id.wallet_last_access);
        walletNotesPreviewTextView = view.findViewById(R.id.wallet_notes_preview);
    }

    private void setOnClickListeners() {
        walletDeleteButton.setOnClickListener(v -> new MaterialAlertDialogBuilder(getContext())
                .setTitle("Delete Wallet")
                .setMessage("Are you sure you want to delete this wallet?\n\nNote: all of the certificates will also be deleted that are associated with this wallet.")
                .setPositiveButton("Delete", (dialog, which) -> {
                    KeyStoreService keyStoreService = KeyStoreService.getInstance();
                    keyStoreService.deleteAlias(selectedWalletAlias);

                    WalletRepository walletRepository = WalletRepository.getInstance(getActivity().getApplication());
                    walletRepository.delete(selectedWalletAlias);

                    Toast.makeText(getContext(), "Wallet deleted",
                            Toast.LENGTH_SHORT)
                            .show();
                    getActivity().finish();
                })
                .setNeutralButton("Go back", null)
                .show());
    }
}