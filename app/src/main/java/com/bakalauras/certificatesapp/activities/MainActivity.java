package com.bakalauras.certificatesapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.adapters.WalletListAdapter;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.WalletRepository;
import com.bakalauras.certificatesapp.services.BiometricService;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.security.KeyPair;
import java.security.KeyStore;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.Executor;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;

public class MainActivity extends AppCompatActivity {

    private boolean walletFabMenuClicked = false;

    private BiometricService biometricService;

    // Floating Action button animations
    private Animation rotateOpen;
    private Animation rotateClose;
    private Animation fromBottom;
    private Animation toBottom;

    private FloatingActionButton walletMenuFab;
    private FloatingActionButton walletCreateFab;
    private TextView walletCreateFabLabel;
    private TextView walletsCountLabel;
    private ListView walletListView;

    private WalletListAdapter walletListAdapter;
    private BiometricPrompt biometricPrompt;

    private String selectedWalletAlias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        biometricService = BiometricService.getInstance(this);

        setActionBar();
        setAnimations();
        setActivityViews();
        setOnClickListeners();
        setBiometricPrompt();
        loadWalletList();
    }

    private void setActionBar() {
        ActionBar bar = ((AppCompatActivity)this).getSupportActionBar();
        bar.setTitle("Wallets");
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.design_default_color_primary)));
    }

    private void openWalletDataActivity() {
        Intent intent = new Intent(MainActivity.this, WalletDataActivity.class);
        intent.putExtra("selectedWalletAlias", selectedWalletAlias);
        startActivity(intent);
    }

    private void loadWalletList() {
        try {
            ListView walletListView  = findViewById(R.id.wallet_list_view);
            // Get the ViewModel.
            WalletViewModel model = new ViewModelProvider(this).get(WalletViewModel.class);

            // Create the observer which updates the UI.
            final Observer<List<WalletEntity>> walletListObserver = wallets -> {
                walletListAdapter = new WalletListAdapter(getApplicationContext(), wallets);
                walletListView.setAdapter(walletListAdapter);
                setWalletsCount(walletListAdapter.getCount());
            };
            model.getAllWallets().observe(this, walletListObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setWalletsCount(int walletsCount) {
        switch (walletsCount){
            case 0:
                walletsCountLabel.setText("No wallets found");
                break;
            case 1:
                walletsCountLabel.setText("1 wallet shown");
                break;
            default:
                walletsCountLabel.setText(String.format("%s wallets shown", walletsCount));
        }
    }

    private void setBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(MainActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(getApplicationContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                openWalletDataActivity();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void setOnClickListeners() {
        // Handle Menu Floating Action button on click
        walletMenuFab.setOnClickListener(v -> {
            walletFabMenuClicked = !walletFabMenuClicked;
            changeActivityViewsVisibility();
            changeActivityViewsClickable();
            changeActivityViewsAnimations();
        });

        // Handle Wallet Create Floating Action button on click
        walletCreateFab.setOnClickListener(v -> {
            Intent intent = new Intent(this, WalletCreateActivity.class);
            startActivity(intent);
        });

        // Set wallet list on click listener
        walletListView.setOnItemClickListener((parent, view, i, l) -> {
            // Prompt appears when user clicks "Log in".
            // Consider integrating with the keystore to unlock cryptographic operations,
            // if needed by your app.
            if(biometricService.canAuthenticate()) {
                selectedWalletAlias = walletListAdapter.getItem(i).getWalletName();
                BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                        .setTitle("Wallet authentication")
                        .setSubtitle("Use biometric authentication to access selected wallet")
                        .setNegativeButtonText("Close")
                        .setAllowedAuthenticators(BIOMETRIC_STRONG)
                        .setConfirmationRequired(false)
                        .build();
                biometricPrompt.authenticate(promptInfo);
            } else {
                Toast.makeText(getApplicationContext(),
                        "Please setup your biometric authentication", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void setAnimations(){
        rotateOpen = AnimationUtils.loadAnimation(this, R.anim.rotate_open_anim);
        rotateClose = AnimationUtils.loadAnimation(this, R.anim.rotate_close_anim);
        fromBottom = AnimationUtils.loadAnimation(this, R.anim.from_bottom_anim);
        toBottom = AnimationUtils.loadAnimation(this, R.anim.to_bottom_anim);
    }

    private void setActivityViews() {
        walletMenuFab = findViewById(R.id.wallet_menu_fab);
        walletCreateFab = findViewById(R.id.wallet_create_fab);
        walletCreateFabLabel = findViewById(R.id.wallet_create_fab_label);
        walletListView = findViewById(R.id.wallet_list_view);
        walletsCountLabel = findViewById(R.id.wallets_count_label);
    }

    private void changeActivityViewsVisibility() {
        if(walletFabMenuClicked) {
            walletCreateFab.setVisibility(View.VISIBLE);
            walletCreateFabLabel.setVisibility(View.VISIBLE);
        } else {
            walletCreateFab.setVisibility(View.INVISIBLE);
            walletCreateFabLabel.setVisibility(View.INVISIBLE);
        }
    }

    private void changeActivityViewsClickable() {
        walletCreateFab.setClickable(walletFabMenuClicked);
    }

    private void changeActivityViewsAnimations() {
        if(walletFabMenuClicked) {
            walletCreateFab.startAnimation(fromBottom);
            walletCreateFabLabel.startAnimation(fromBottom);
            walletMenuFab.startAnimation(rotateOpen);
        } else {
            walletCreateFab.startAnimation(toBottom);
            walletCreateFabLabel.startAnimation(toBottom);
            walletMenuFab.startAnimation(rotateClose);
        }
    }
}