package com.bakalauras.certificatesapp.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.CertificateRepository;
import com.bakalauras.certificatesapp.repositories.WalletRepository;
import com.bakalauras.certificatesapp.services.CRTObject;
import com.bakalauras.certificatesapp.services.CSRService;
import com.bakalauras.certificatesapp.services.KeyStoreService;
import com.bakalauras.certificatesapp.view_models.CertificateViewModel;
import com.bakalauras.certificatesapp.view_models.CertificateViewModelFactory;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.openssl.PEMWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class CertificateDetailsActivity extends AppCompatActivity {

    private String selectedWalletAlias;
    private String selectedCertificateName;

    private ImageView copyToClipboardImageView;
    private Toolbar certificateDetailsToolbar;
    private TextView certificateKeyUsagesTextView;
    private TextView certificateKeyCritialTextView;
    private TextView certificateIssuedAtTextView;
    private TextView certificateLastAccessedAtTextView;
    private TextView certificateMSPIdTextView;
    private TextView certificateTypeTextView;
    private TextView certificateIssuedVersionTextView;
    private TextView certificateIssuedNotValidBeforeTextView;
    private TextView certificateIssuedNotValidAfterTextView;
    private TextView certificatePemFormatTextView;
    private TextView cnSubjectTextView;
    private TextView cSubjectTextView;
    private TextView stSubjectTextView;
    private TextView lSubjectTextView;
    private TextView oSubjectTextView;
    private TextView ouSubjectTextView;
    private TextView cnIssuerTextView;
    private TextView cIssuerTextView;
    private TextView stIssuerTextView;
    private TextView lIssuerTextView;
    private TextView oIssuerTextView;
    private TextView ouIssuerTextView;
    private MaterialButton certificateDeleteButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_certificate_details);

        selectedWalletAlias = getIntent().getExtras().getString("selectedWalletAlias");
        selectedCertificateName = getIntent().getExtras().getString("selectedCertificateName");

        setActivityViews();
        setOnClickListeners();
        loadCertificateData();
    }

    private void loadCertificateData() {
        CertificateViewModelFactory factory = new CertificateViewModelFactory(getApplication(), selectedWalletAlias);
        CertificateViewModel model = new ViewModelProvider(this, factory).get(CertificateViewModel.class);

        final Observer<CertificateEntity> certificateObserver = certificate -> {
            try {
                certificate.setAccessedAt(new Date(System.currentTimeMillis()));
                CertificateRepository.getInstance(getApplication(), selectedWalletAlias).update(certificate);
                setCertificateData(certificate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        model.getSelectedCertificate(selectedWalletAlias, selectedCertificateName).observe(this, certificateObserver);
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    private void setCertificateData(CertificateEntity certificate) throws CertificateEncodingException {
        X509Certificate keystoreCertificate = KeyStoreService.getInstance().getCertificate(certificate.getCertificateName());

        String notValidBefore = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(keystoreCertificate.getNotBefore());
        String notValidAfter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(keystoreCertificate.getNotAfter());

        CRTObject crtObject = new CRTObject(keystoreCertificate);

        certificateIssuedAtTextView.setText(certificate.getIssuedAt().toString());
        certificateLastAccessedAtTextView.setText(certificate.getAccessedAt().toString());
        certificateMSPIdTextView.setText(certificate.getMspId());
        certificateTypeTextView.setText(certificate.getType());

        certificateIssuedNotValidBeforeTextView.setText(notValidBefore);
        certificateIssuedNotValidAfterTextView.setText(notValidAfter);
        certificateIssuedVersionTextView.setText(String.valueOf(keystoreCertificate.getVersion()));

        cnSubjectTextView.setText(crtObject.getSubjectString(BCStyle.CN));
        cSubjectTextView.setText(crtObject.getSubjectString(BCStyle.C));
        stSubjectTextView.setText(crtObject.getSubjectString(BCStyle.ST));
        lSubjectTextView.setText(crtObject.getSubjectString(BCStyle.L));
        oSubjectTextView.setText(crtObject.getSubjectString(BCStyle.O));
        ouSubjectTextView.setText(crtObject.getSubjectString(BCStyle.OU));

        cnIssuerTextView.setText(crtObject.getIssuerString(BCStyle.CN));
        cIssuerTextView.setText(crtObject.getIssuerString(BCStyle.C));
        stIssuerTextView.setText(crtObject.getIssuerString(BCStyle.ST));
        lIssuerTextView.setText(crtObject.getIssuerString(BCStyle.L));
        oIssuerTextView.setText(crtObject.getIssuerString(BCStyle.O));
        ouIssuerTextView.setText(crtObject.getIssuerString(BCStyle.OU));
        certificateKeyUsagesTextView.setText(crtObject.getKeyUsageUsages());
        certificateKeyCritialTextView.setText("true");

        certificatePemFormatTextView.setText(crtObject.toPem());
    }

    private void setActivityViews() {
        certificateDetailsToolbar = findViewById(R.id.certificate_details_toolbar);
        certificateIssuedAtTextView = findViewById(R.id.crt_issued_at);
        certificateLastAccessedAtTextView = findViewById(R.id.crt_last_accessed_at);
        certificateMSPIdTextView = findViewById(R.id.crt_msp);
        certificateTypeTextView = findViewById(R.id.crt_type);
        certificatePemFormatTextView = findViewById(R.id.crt_pem_format_review);
        cnSubjectTextView = findViewById(R.id.cn_review);
        cSubjectTextView = findViewById(R.id.c_review);
        stSubjectTextView = findViewById(R.id.st_review);
        lSubjectTextView = findViewById(R.id.l_review);
        oSubjectTextView = findViewById(R.id.o_review);
        ouSubjectTextView = findViewById(R.id.ou_review);
        certificateIssuedVersionTextView = findViewById(R.id.crt_issued_version);
        certificateIssuedNotValidBeforeTextView = findViewById(R.id.crt_issued_not_valid_before);
        certificateIssuedNotValidAfterTextView = findViewById(R.id.crt_issued_not_valid_after);
        certificatePemFormatTextView = findViewById(R.id.crt_pem_format_review);
        copyToClipboardImageView = findViewById(R.id.copy_to_clipboard_icon);
        certificateKeyUsagesTextView = findViewById(R.id.crt_key_usage_usages);
        certificateKeyCritialTextView = findViewById(R.id.crt_key_usage_critical);
        certificateDeleteButton = findViewById(R.id.certificate_delete_button);

        cnIssuerTextView = findViewById(R.id.issuer_cn_review);
        cIssuerTextView = findViewById(R.id.issuer_c_review);
        stIssuerTextView = findViewById(R.id.issuer_st_review);
        lIssuerTextView = findViewById(R.id.issuer_l_review);
        oIssuerTextView = findViewById(R.id.issuer_o_review);
        ouIssuerTextView = findViewById(R.id.issuer_ou_review);
    }

    private void setOnClickListeners() {
        certificateDetailsToolbar.setNavigationOnClickListener(v -> finish());

        copyToClipboardImageView.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("crtInPemFormat", certificatePemFormatTextView.getText());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
        });

        certificateDeleteButton.setOnClickListener(v -> new MaterialAlertDialogBuilder(CertificateDetailsActivity.this)
                .setTitle("Delete Certificate")
                .setMessage("Are you sure you want to delete this certificate?")
                .setPositiveButton("Delete", (dialog, which) -> {
                    KeyStoreService keyStoreService = KeyStoreService.getInstance();
                    keyStoreService.deleteCertificate(selectedCertificateName);

                    CertificateRepository certificateRepository = CertificateRepository.getInstance(getApplication(), selectedWalletAlias);
                    certificateRepository.delete(selectedWalletAlias, selectedCertificateName);

                    Toast.makeText(getApplicationContext(), "Certificate deleted",
                            Toast.LENGTH_SHORT)
                            .show();
                    this.finish();
                })
                .setNeutralButton("Go back", null)
                .show());
    }

}
