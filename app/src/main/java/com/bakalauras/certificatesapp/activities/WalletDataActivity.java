package com.bakalauras.certificatesapp.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.adapters.WalletDataViewPagerAdapter;
import com.bakalauras.certificatesapp.adapters.WalletListAdapter;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.WalletRepository;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.Executor;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;

public class WalletDataActivity extends FragmentActivity {

    private ViewPager2 walletDataViewPager;
    private BottomNavigationView walletDataNavigation;
    private Toolbar walletDataToolbar;
    private String selectedWalletAlias;
    private BiometricPrompt biometricPrompt;

    private WalletViewModel walletViewModel;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_data);

        setBiometricPrompt();

        selectedWalletAlias = getIntent().getExtras().getString("selectedWalletAlias");

        final Observer<WalletEntity> walletObserver = wallet -> {
            try {
                wallet.setAccessedAt(new Date(System.currentTimeMillis()));
                WalletRepository.getInstance(getApplication()).update(wallet);
                // When wallet is retrieved we can set all of the remaining resources
                // this way we will not get NullPointerException
                if (walletDataViewPager == null) {
                    setActivityViews();
                    setAdapters();
                    setOnClickListeners();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        walletViewModel = new ViewModelProvider(this).get(WalletViewModel.class);
        walletViewModel.getSelectedWallet(selectedWalletAlias).observe(this, walletObserver);
    }

    private void setBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(WalletDataActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
                finish();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(getApplicationContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
                finish();
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    private void setOnClickListeners() {
        // Wallet data activity on toolbar back button clicked
        walletDataToolbar.setNavigationOnClickListener(v -> finish());

        walletDataViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @SuppressLint("ResourceType")
            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        walletDataNavigation.setSelectedItemId(R.id.wallet_bottom_navigation_wallet);
                        break;
                    case 1:
                        walletDataNavigation.setSelectedItemId(R.id.wallet_bottom_navigation_certificates);
                        break;
//                    case 2:
//                        walletDataNavigation.setSelectedItemId(R.id.wallet_bottom_navigation_transactions);
//                        break;
                }
            }
        });

        walletDataNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.wallet_bottom_navigation_wallet:
                    walletDataViewPager.setCurrentItem(0);
                    break;
                case R.id.wallet_bottom_navigation_certificates:
                    walletDataViewPager.setCurrentItem(1);
                    break;
//                case R.id.wallet_bottom_navigation_transactions:
//                    walletDataViewPager.setCurrentItem(2);
//                    break;
            }
            return true;
        });
    }

    private void setAdapters() {
        walletDataViewPager.setAdapter(new WalletDataViewPagerAdapter(this, selectedWalletAlias));
    }

    private void setActivityViews() {
        walletDataViewPager = findViewById(R.id.wallet_data_view_pager);
        walletDataNavigation = findViewById(R.id.wallet_data_bottom_navigation);
        walletDataToolbar = findViewById(R.id.wallet_data_toolbar);
    }

    @Override
    public void onBackPressed() {
        if (walletDataViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            walletDataViewPager.setCurrentItem(walletDataViewPager.getCurrentItem() - 1);
        }
    }
}
