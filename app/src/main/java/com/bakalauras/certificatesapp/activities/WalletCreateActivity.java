package com.bakalauras.certificatesapp.activities;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.security.keystore.KeyProperties;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.WalletRepository;
import com.bakalauras.certificatesapp.services.KeyStoreService;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;

import java.security.KeyStore;
import java.sql.Date;
import java.util.Enumeration;

public class WalletCreateActivity extends AppCompatActivity {

    private Button createWalletButton;
    private EditText walletNameEditText;
    private EditText walletNotesEditText;
    private WalletViewModel walletViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_create);

        setActivityViews();
        setOnClickListeners();
    }

    private void setActivityViews() {
        createWalletButton = findViewById(R.id.wallet_create_button);
        walletNameEditText = findViewById(R.id.wallet_name_edit_text);
        walletNotesEditText = findViewById(R.id.wallet_notes_input);
    }

    private void setOnClickListeners() {
        createWalletButton.setOnClickListener(v -> {
            String walletNameValue = walletNameEditText.getText().toString().trim();
            String walletNotesValue = walletNotesEditText.getText().toString().trim();
            boolean formIsValid = createFormIsValid(walletNameValue);

            if (formIsValid) {
                // Form is valid and we can create the wallet.
                try {
                    KeyStoreService.getInstance().generateSecretKey(walletNameValue);
                    WalletRepository dbrepo = WalletRepository.getInstance(getApplication());

                    WalletEntity wallet = new WalletEntity();
                    wallet.setWalletName(walletNameValue);
                    wallet.setCreatedAt(new Date(System.currentTimeMillis()));
                    wallet.setNote(walletNotesValue);
                    dbrepo.insert(wallet);

                    Toast.makeText(getApplicationContext(),"Wallet was successfully created!", Toast.LENGTH_LONG).show();

                    // Close activity when wallet will be added
                    final Observer<WalletEntity> walletObserver = _wallet -> {
                        finish();
                    };

                    walletViewModel = new ViewModelProvider(this).get(WalletViewModel.class);
                    walletViewModel.getSelectedWallet(walletNameValue).observe(this, walletObserver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private boolean createFormIsValid(String walletName) {
        if (walletName.equals("")) {
            walletNameEditText.setError("Wallet name is required!");
            return false;
        }

        // Check if wallet name does not already exist
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            Enumeration<String> aliases = ks.aliases();

            // Check if the wallet name does not already exist
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                if(alias.equals(walletName.trim())) {
                    walletNameEditText.setError("Wallet with this name already exist!");
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}