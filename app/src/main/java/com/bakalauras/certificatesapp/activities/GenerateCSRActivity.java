package com.bakalauras.certificatesapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;

import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.FragmentActivity;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.services.CSRService;
import com.bakalauras.certificatesapp.services.CSRObject;
import com.bakalauras.certificatesapp.services.KeyStoreService;
import com.google.android.material.textfield.TextInputEditText;

import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.io.IOException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

public class GenerateCSRActivity extends FragmentActivity {

    private Toolbar generateCsrToolbar;
    private Button generateCsrButton;
    private String selectedWalletAlias;
    private TextInputEditText commonNameEditText;
    private TextInputEditText countryEditText;
    private TextInputEditText stateEditText;
    private TextInputEditText locationEditText;
    private TextInputEditText organizationEditText;
    private TextInputEditText organizationUnitEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_csr);

        selectedWalletAlias = getIntent().getExtras().getString("selectedWalletAlias");

        setActivityViews();
        setOnClickListeners();
    }

    private void setActivityViews() {
        generateCsrButton = findViewById(R.id.generate_csr_button);
        commonNameEditText = findViewById(R.id.common_name_input);
        countryEditText = findViewById(R.id.country_input);
        stateEditText = findViewById(R.id.state_input);
        locationEditText = findViewById(R.id.location_input);
        organizationEditText = findViewById(R.id.organization_input);
        organizationUnitEditText = findViewById(R.id.organization_unit_input);

        generateCsrToolbar = findViewById(R.id.generate_csr_toolbar);
    }
    private void setOnClickListeners() {
        generateCsrToolbar.setNavigationOnClickListener(v -> finish());

        generateCsrButton.setOnClickListener(v -> {
            if(generateFormIsValid()) {
                KeyPair keyPair = KeyStoreService.getInstance().getKeyPair(selectedWalletAlias);
                CSRObject csrObject = new CSRObject(
                        commonNameEditText.getText().toString(),
                        countryEditText.getText().toString(),
                        stateEditText.getText().toString(),
                        locationEditText.getText().toString(),
                        organizationEditText.getText().toString(),
                        organizationUnitEditText.getText().toString()
                );

                try {
                    PKCS10CertificationRequest csr = CSRService.getInstance().generateCSR(keyPair, csrObject);
                    String csrPem = CSRService.getInstance().csrToPem(csr);
                    Intent intent = new Intent(getApplicationContext(), ValidateCSRActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    intent.putExtra("csrPem", csrPem);
                    intent.putExtra("selectedWalletAlias", selectedWalletAlias);
                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean generateFormIsValid() {
        boolean valid = true;
        List<TextInputEditText> inputList = new ArrayList<TextInputEditText>() {{
            add(commonNameEditText);
            add(countryEditText);
            add(stateEditText);
            add(locationEditText);
            add(organizationEditText);
        }};

        for(int i = 0; i < inputList.size(); i++){
            String value = inputList.get(i).getText().toString();
            if (TextUtils.isEmpty(value)) {
                inputList.get(i).setError("This field is required");
                valid = false;
            }
        }

        return valid;
    }
}
