package com.bakalauras.certificatesapp.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bakalauras.certificatesapp.Constants;
import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.adapters.CertificateListAdapter;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.CertificateRepository;
import com.bakalauras.certificatesapp.services.CSRService;
import com.bakalauras.certificatesapp.services.CSRObject;
import com.bakalauras.certificatesapp.services.KeyStoreService;
import com.bakalauras.certificatesapp.view_models.CertificateViewModel;
import com.bakalauras.certificatesapp.view_models.CertificateViewModelFactory;
import com.bakalauras.certificatesapp.view_models.WalletViewModel;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.asn1.x500.RDN;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.asn1.x500.style.IETFUtils;
import org.spongycastle.operator.ContentVerifierProvider;
import org.spongycastle.operator.OperatorCreationException;
import org.spongycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.PKCSException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.Date;
import java.util.Base64;
import java.util.List;

import javax.security.auth.x500.X500Principal;

public class ValidateCSRActivity extends AppCompatActivity {

    private ProgressBar csrIssueProgressBar;
    private Toolbar validateCsrToolbar;
    private TextView csrPemFormatReviewTextView;
    private TextView cnReviewTextView;
    private TextView cReviewTextView;
    private TextView stReviewTextView;
    private TextView lReviewTextView;
    private TextView oReviewTextView;
    private TextView ouReviewTextView;
    private TextView publicKeyAlgorithmReviewTextView;
    private TextView publicKeySizeReviewTextView;
    private TextView signatureAlgorithmReviewTextView;
    private TextView signatureReviewTextView;
    private TextView signatureValidityReviewTextView;
    private ImageView copyToClipboardImageView;
    private Button issueCRTButton;

    private String csrPem;
    private String selectedWalletAlias;
    private PKCS10CertificationRequest csr;

    private final FirebaseFirestore firebaseDB = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_validate_csr);
        setActivityViews();
        setOnClickListeners();

        try {
            csrPem = getIntent().getExtras().getString("csrPem");
            selectedWalletAlias = getIntent().getExtras().getString("selectedWalletAlias");
            csr = CSRService.getInstance().csrFromPem(csrPem);

            setTextViewValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean connectedToInternet() {
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting());
    }

    @SuppressLint("SetTextI18n")
    private void setTextViewValues() throws OperatorCreationException, PKCSException {
        CSRObject csrObject = new CSRObject();
        csrObject.fromX500Name(csr.getSubject());

        byte[] encodedSignature = Base64.getEncoder().encode(csr.getSignature());

        csrPemFormatReviewTextView.setText(csrPem);
        publicKeyAlgorithmReviewTextView.setText("Elliptic Curve");
        publicKeySizeReviewTextView.setText("256 bits");
        cnReviewTextView.setText(csrObject.getCommonName());
        cReviewTextView.setText(csrObject.getCountry());
        stReviewTextView.setText(csrObject.getState());
        lReviewTextView.setText(csrObject.getLocality());
        oReviewTextView.setText(csrObject.getOrganization());
        ouReviewTextView.setText(csrObject.getOrganizationalUnit());
        signatureAlgorithmReviewTextView.setText("ECDSA");
        signatureReviewTextView.setText(new String(encodedSignature));
        signatureValidityReviewTextView.setText(String.format("%s", isSignatureValid()));
    }

    private boolean isSignatureValid() throws OperatorCreationException, PKCSException {
        KeyPair keyPair = KeyStoreService.getInstance().getKeyPair(selectedWalletAlias);
        ContentVerifierProvider verifyProvider = new JcaContentVerifierProviderBuilder().build(keyPair.getPublic());
        return csr.isSignatureValid(verifyProvider);
    }

    private void setActivityViews() {
        validateCsrToolbar = findViewById(R.id.validate_csr_toolbar);
        csrPemFormatReviewTextView = findViewById(R.id.csr_pem_format_review);
        cnReviewTextView = findViewById(R.id.cn_review);
        cReviewTextView = findViewById(R.id.c_review);
        stReviewTextView = findViewById(R.id.st_review);
        lReviewTextView = findViewById(R.id.l_review);
        oReviewTextView = findViewById(R.id.o_review);
        ouReviewTextView = findViewById(R.id.ou_review);
        publicKeyAlgorithmReviewTextView = findViewById(R.id.public_key_algorithm_review);
        publicKeySizeReviewTextView = findViewById(R.id.public_key_size_review);
        signatureAlgorithmReviewTextView = findViewById(R.id.signature_algorithm_review);
        signatureReviewTextView = findViewById(R.id.signature_review);
        signatureValidityReviewTextView = findViewById(R.id.signature_validity_review);
        copyToClipboardImageView = findViewById(R.id.copy_to_clipboard_icon);
        issueCRTButton = findViewById(R.id.issue_crt_button);
        csrIssueProgressBar = findViewById(R.id.csr_issue_progress_bar);
    }

    private void setOnClickListeners() {
        validateCsrToolbar.setNavigationOnClickListener(v -> finish());

        issueCRTButton.setOnClickListener(v -> {
            if(!connectedToInternet()) {
                Toast.makeText(getApplicationContext(), "Error: no internet connection", Toast.LENGTH_SHORT).show();
            } else {
                changeLoadingStatus(true);

                // Get Fabric-CA API url
                firebaseDB.collection(Constants.FIREBASE_COLLECTION).document(Constants.FIREBASE_CONFIG_DOCUMENT)
                        .get()
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                assert document != null;
                                if (document.exists()) {
                                    String API_URL = document.getData().get("CRT_ENROLL_API_URL").toString();

                                    JSONObject jsonBody = new JSONObject();
                                    try {
                                        jsonBody.put("csr", csrPem.replaceAll("\n","\\\n"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, API_URL, jsonBody,
                                        response -> {
                                            try {
                                                CertificateRepository certificateRepository = CertificateRepository.getInstance(getApplication(), selectedWalletAlias);
                                                KeyStoreService keyStoreService = KeyStoreService.getInstance();

                                                String crtPem = response.getJSONObject("credentials").getString("certificate");
                                                String mspId = response.getString("mspId");
                                                String type = response.getString("type");
                                                String commonName = getCertificateCommonName(crtPem);

                                                // Store certificate inside keystore
                                                keyStoreService.storeCertificate(crtPem, commonName);

                                                // Generate certificate object and store it inside local database
                                                CertificateEntity certificateEntity = new CertificateEntity();
                                                certificateEntity.setWallet(selectedWalletAlias);

                                                assert commonName != null;
                                                certificateEntity.setCertificateName(commonName);
                                                certificateEntity.setMspId(mspId);
                                                certificateEntity.setType(type);
                                                certificateEntity.setIssuedAt(new Date(System.currentTimeMillis()));

                                                certificateRepository.insert(certificateEntity);

                                                Toast.makeText(getApplicationContext(), "Certificate was successfully issued", Toast.LENGTH_SHORT).show();

                                                CertificateViewModelFactory factory = new CertificateViewModelFactory(getApplication(), selectedWalletAlias);
                                                CertificateViewModel model = new ViewModelProvider(this, factory).get(CertificateViewModel.class);

                                                // Create observer that will check when it is safe to close the activity
                                                final Observer<CertificateEntity> certificateListObserver = _cert -> {
                                                    Intent intent = new Intent(this, WalletDataActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    intent.putExtra("selectedWalletAlias", selectedWalletAlias);
                                                    startActivity(intent);
                                                };
                                                model.getSelectedCertificate(selectedWalletAlias, commonName).observe(this, certificateListObserver);
                                            }catch (Exception e){
                                                Toast.makeText(getApplicationContext(), String.format("Error: %s", e), Toast.LENGTH_SHORT).show();
                                                e.printStackTrace();
                                            }

                                            changeLoadingStatus(false);
                                        },
                                        error -> {
                                            if(error.networkResponse.statusCode == 422) {
                                                Toast.makeText(getApplicationContext(), "Common name is already occupied", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), String.format("Error: %s", error), Toast.LENGTH_SHORT).show();
                                            }
                                            changeLoadingStatus(false);
                                        }
                                    );
                                    requestQueue.add(jsonObjectRequest);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Issue with firebase. Contact the administrations", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Issue with firebase. Contact the administrations", Toast.LENGTH_SHORT).show();
                            }
                        });
            }

        });
        copyToClipboardImageView.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("csrInPemFormat", csrPemFormatReviewTextView.getText());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
        });
    }

    private String getCertificateCommonName(String crtPem) {
        try {
            InputStream targetStream = new ByteArrayInputStream(crtPem.getBytes());
            X509Certificate cert = (X509Certificate) CertificateFactory
                    .getInstance("X509")
                    .generateCertificate(targetStream);

            // Get Common name from the certificate object
            X500Principal principal = cert.getSubjectX500Principal();

            X500Name x500name = new X500Name(principal.getName());
            RDN cn = x500name.getRDNs(BCStyle.CN)[0];

            return IETFUtils.valueToString(cn.getFirst().getValue());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void changeLoadingStatus(boolean loading) {
        if (loading) {
            csrIssueProgressBar.setVisibility(View.VISIBLE);
        } else {
            csrIssueProgressBar.setVisibility(View.INVISIBLE);
        }

        issueCRTButton.setEnabled(!loading);
    }
}
