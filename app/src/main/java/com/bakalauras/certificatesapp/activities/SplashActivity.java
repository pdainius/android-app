package com.bakalauras.certificatesapp.activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.bakalauras.certificatesapp.R;

public class SplashActivity extends AppCompatActivity {

    // Splash screen duration
    private final int SPLASH_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Initialise splash screen with some animations
        new Handler().postDelayed(() -> {
            Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation);
            SplashActivity.this.finish();
        }, SPLASH_TIME);
    }
}
