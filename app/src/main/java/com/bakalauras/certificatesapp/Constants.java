package com.bakalauras.certificatesapp;

import android.security.keystore.KeyProperties;

public class Constants {
    public static final String DB_NAME = "cert_warden_db";
    public static final String KEYSTORE_PROVIDER = "AndroidKeyStore";
    public static final String KEYSTORE_KEY_ALGORITHM = KeyProperties.KEY_ALGORITHM_EC;
    public static final String EC_CURVE = "secp256r1";
    public static final String FIREBASE_COLLECTION = "certwarden";
    public static final String FIREBASE_CONFIG_DOCUMENT = "app_configs";
    public static final String KEYSTORE_CERTIFICATE_ALIAS_PREFIX = "cert";
}
