package com.bakalauras.certificatesapp.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.bakalauras.certificatesapp.database.AppDatabase;
import com.bakalauras.certificatesapp.database.dao.CertificateDao;
import com.bakalauras.certificatesapp.database.dao.WalletDao;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;

import java.util.List;

public class CertificateRepository {
    private static CertificateRepository instance;
    private final AppDatabase database;
    private String walletAlias;

    private final CertificateDao certificateDao;

    private LiveData<List<CertificateEntity>> certificateList;
    private LiveData<CertificateEntity> selectedCertificate;

    private CertificateRepository(Application app, String walletAlias) {
        this.database = AppDatabase.getInstance(app.getApplicationContext());
        this.certificateDao = database.certificateDao();
        this.certificateList = certificateDao.getAllCertificatesByWalletID(walletAlias);
        this.walletAlias = walletAlias;
    }

    public static CertificateRepository getInstance(Application app, String walletAlias) {
        if (instance == null || !instance.walletAlias.equals(walletAlias)) {
            instance = new CertificateRepository(app, walletAlias);
        }

        return instance;
    }

    public void insert(CertificateEntity certificate) {
        database.getDbExecutor().execute(() -> {
            certificateDao.insert(certificate);
        });
    }

    public void update(CertificateEntity certificate) {
        database.getDbExecutor().execute(() -> {
            certificateDao.update(certificate);
        });
    }

    public void delete(String walletAlias, String certificateName) {
        database.getDbExecutor().execute(() -> {
            certificateDao.delete(walletAlias, certificateName);
        });
    }

    public LiveData<List<CertificateEntity>> getAllCertificates() {
        return certificateList;
    }

    public LiveData<CertificateEntity> getCertificate(String walletName, String certificateName) {
        selectedCertificate = certificateDao.getCertificateByName(certificateName, walletName);
        return selectedCertificate;
    }
}
