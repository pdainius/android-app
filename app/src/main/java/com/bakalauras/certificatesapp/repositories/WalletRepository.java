package com.bakalauras.certificatesapp.repositories;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;

import androidx.lifecycle.LiveData;

import com.bakalauras.certificatesapp.database.AppDatabase;
import com.bakalauras.certificatesapp.database.dao.WalletDao;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;

import java.util.List;

public class WalletRepository {
    private static WalletRepository instance;
    private final AppDatabase database;

    private final WalletDao walletDao;

    private LiveData<List<WalletEntity>> walletList;
    private LiveData<WalletEntity> selectedWallet;

    private WalletRepository(Application app) {
        database = AppDatabase.getInstance(app.getApplicationContext());
        walletDao = database.walletDao();
        walletList = walletDao.getAllWallets();
    }

    public static WalletRepository getInstance(Application app) {
        if (instance == null) {
            instance = new WalletRepository(app);
        }

        return instance;
    }

    public void insert(WalletEntity wallet) {
        database.getDbExecutor().execute(() -> {
            walletDao.insert(wallet);
        });
    }

    public void update(WalletEntity wallet) {
        database.getDbExecutor().execute(() -> {
            walletDao.update(wallet);
        });
    }

    public void delete(String walletName) {
        database.getDbExecutor().execute(() -> {
            walletDao.delete(walletName);
        });
    }

    public LiveData<List<WalletEntity>> getAllWallets() {
        return walletList;
    }
    public LiveData<WalletEntity> getWalletByName(String walletName) {
        selectedWallet = walletDao.getWalletByName(walletName);
        return selectedWallet;
    }
}
