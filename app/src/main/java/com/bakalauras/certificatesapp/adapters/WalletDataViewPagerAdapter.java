package com.bakalauras.certificatesapp.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.fragments.CertificatesListFragment;
import com.bakalauras.certificatesapp.fragments.TransactionsFragment;
import com.bakalauras.certificatesapp.fragments.WalletDetailsFragment;

public class WalletDataViewPagerAdapter extends FragmentStateAdapter {

    private static final int NUM_PAGES = 2;
    private final String selectedWalletAlias;

    public WalletDataViewPagerAdapter(FragmentActivity fa, String selectedWalletAlias) {
        super(fa);
        this.selectedWalletAlias = selectedWalletAlias;
    }

    @Override
    public Fragment createFragment(int position) {
        Bundle args = new Bundle();
        args.putString("selectedWalletAlias", selectedWalletAlias);
        switch (position){
            case 0:
                WalletDetailsFragment fragment1 = new WalletDetailsFragment();
                fragment1.setArguments(args);
                return fragment1;
            case 1:
                CertificatesListFragment fragment2 = new CertificatesListFragment();
                fragment2.setArguments(args);
                return fragment2;
//            case 2:
//                TransactionsFragment fragment3 = new TransactionsFragment();
//                fragment3.setArguments(args);
//                return fragment3;
        }
        return new WalletDetailsFragment();
    }

    @Override
    public int getItemCount() {
        return NUM_PAGES;
    }
}
