package com.bakalauras.certificatesapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.view_holders.CertificateListViewHolder;

import java.util.List;

public class CertificateListAdapter extends BaseAdapter {

    private final Context context;
    private List<CertificateEntity> certificates;

    public CertificateListAdapter(Context context, List<CertificateEntity> certificates) {
        this.context = context;
        this.certificates = certificates;
    }

    @Override
    public int getCount() {
        return certificates.size();
    }

    @Override
    public CertificateEntity getItem(int position) {
        return certificates.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CertificateListViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.certificate_list_view_row_items, parent, false);
            viewHolder = new CertificateListViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CertificateListViewHolder) convertView.getTag();
        }

        CertificateEntity certificate = certificates.get(position);
        viewHolder.setCertificateName(certificate.getCertificateName());

        if(certificate.getAccessedAt() == null) {
            viewHolder.setCertificateLastAccessed("N/A");
        } else {
            viewHolder.setCertificateLastAccessed(certificate.getAccessedAt().toString());
        }

        return convertView;
    }
}
