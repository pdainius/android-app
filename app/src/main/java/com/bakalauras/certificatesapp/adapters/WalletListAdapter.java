package com.bakalauras.certificatesapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bakalauras.certificatesapp.R;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.view_holders.WalletListViewHolder;

import java.util.List;

public class WalletListAdapter extends BaseAdapter {

    private final Context context;
    private List<WalletEntity> wallets;

    public WalletListAdapter(Context context, List<WalletEntity> wallets) {
        this.context = context;
        this.wallets = wallets;
    }

    @Override
    public int getCount() {
        return wallets.size();
    }

    @Override
    public WalletEntity getItem(int position) {
        return wallets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WalletListViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.wallet_list_view_row_items, parent, false);
            viewHolder = new WalletListViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (WalletListViewHolder) convertView.getTag();
        }

        WalletEntity wallet = wallets.get(position);
        viewHolder.setWalletName(wallet.getWalletName());

        if(wallet.getAccessedAt() == null) {
            viewHolder.setWalletLastAccessed("N/A");
        } else {
            viewHolder.setWalletLastAccessed(wallet.getAccessedAt().toString());
        }

        return convertView;
    }
}