package com.bakalauras.certificatesapp.view_models;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class CertificateViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private String walletAlias;


    public CertificateViewModelFactory(Application application, String walletAlias) {
        this.mApplication = application;
        this.walletAlias = walletAlias;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CertificateViewModel(mApplication, walletAlias);
    }
}
