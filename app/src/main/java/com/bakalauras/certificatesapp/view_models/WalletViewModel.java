package com.bakalauras.certificatesapp.view_models;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.AndroidViewModel;

import com.bakalauras.certificatesapp.database.dao.WalletDao;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.WalletRepository;

import java.util.List;

public class WalletViewModel extends AndroidViewModel {
    private LiveData<List<WalletEntity>> walletList;
    private LiveData<WalletEntity> selectedWallet;

    private final WalletRepository walletRepository;

    public WalletViewModel(Application application) {
        super(application);

        walletRepository = WalletRepository.getInstance(application);
        walletList = walletRepository.getAllWallets();
    }

    public LiveData<List<WalletEntity>> getAllWallets() {
        return walletList;
    }

    public LiveData<WalletEntity> getSelectedWallet(String walletName) {
        return walletRepository.getWalletByName(walletName);
    }
}
