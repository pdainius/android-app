package com.bakalauras.certificatesapp.view_models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;
import com.bakalauras.certificatesapp.repositories.CertificateRepository;
import com.bakalauras.certificatesapp.repositories.WalletRepository;

import java.util.List;

public class CertificateViewModel extends AndroidViewModel {
    private LiveData<List<CertificateEntity>> certificateList;

    private final CertificateRepository certificateRepository;

    public CertificateViewModel(Application application, String walletAlias) {
        super(application);

        certificateRepository = CertificateRepository.getInstance(application, walletAlias);
        certificateList = certificateRepository.getAllCertificates();
    }

    public LiveData<List<CertificateEntity>> getAllCertificates() {
        return certificateList;
    }

    public LiveData<CertificateEntity> getSelectedCertificate(String walletName, String certificateName) {
        return certificateRepository.getCertificate(walletName, certificateName);
    }
}
