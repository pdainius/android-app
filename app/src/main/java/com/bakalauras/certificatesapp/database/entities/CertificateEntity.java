package com.bakalauras.certificatesapp.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import java.sql.Date;

@Entity(
        primaryKeys = {"wallet_id", "certificate_name"},
        foreignKeys = {@ForeignKey(entity = WalletEntity.class,
                parentColumns = "wallet_name",
                childColumns = "wallet_id",
                onDelete = ForeignKey.CASCADE)}
        )
public class CertificateEntity {
    @ColumnInfo(name="wallet_id")
    @NonNull
    private String walletId;

    @ColumnInfo(name="certificate_name")
    @NonNull
    private String certificateName;

    @ColumnInfo(name="msp_id")
    @NonNull
    private String mspId;

    @ColumnInfo(name="type")
    @NonNull
    private String type;

    @ColumnInfo(name="accessed_at")
    private Date accessedAt;

    @ColumnInfo(name="issued_at")
    private Date issuedAt;

    @Ignore
    public CertificateEntity() {}

    public CertificateEntity(@NonNull String walletId, @NonNull String certificateName, @NonNull String mspId, @NonNull String type,  Date accessedAt, Date issuedAt) {
        this.walletId = walletId;
        this.certificateName = certificateName;
        this.mspId = mspId;
        this.type = type;
        this.accessedAt = accessedAt;
        this.issuedAt = issuedAt;
    }

    @NonNull
    public String getWalletId() {
        return walletId;
    }

    public void setWallet(@NonNull String walletId) {
        this.walletId = walletId;
    }

    @NonNull
    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(@NonNull String certificateName) {
        this.certificateName = certificateName;
    }

    @NonNull
    public String getMspId() {
        return mspId;
    }

    public void setMspId(@NonNull String mspId) {
        this.mspId = mspId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getAccessedAt() {
        return accessedAt;
    }

    public void setAccessedAt(Date accessedAt) {
        this.accessedAt = accessedAt;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    @Override
    public String toString() {
        return "CertificateEntity{" +
                "walletId='" + walletId + '\'' +
                ", certificateName='" + certificateName + '\'' +
                ", mspId='" + mspId + '\'' +
                ", type=" + type +
                ", accessedAt=" + accessedAt +
                ", issuedAt=" + issuedAt +
                '}';
    }
}