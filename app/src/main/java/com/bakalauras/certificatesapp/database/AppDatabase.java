package com.bakalauras.certificatesapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.bakalauras.certificatesapp.Constants;
import com.bakalauras.certificatesapp.database.converters.Converters;
import com.bakalauras.certificatesapp.database.dao.CertificateDao;
import com.bakalauras.certificatesapp.database.dao.WalletDao;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {WalletEntity.class, CertificateEntity.class}, version=1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract WalletDao walletDao();
    public abstract CertificateDao certificateDao();

    private static volatile AppDatabase instance = null;

    private final int THREADS = 4;
    public final ExecutorService dbExecutor = Executors.newFixedThreadPool(THREADS);

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, Constants.DB_NAME).build();
        }

        return instance;
    }

    public ExecutorService getDbExecutor() {
        return dbExecutor;
    }
}
