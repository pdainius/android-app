package com.bakalauras.certificatesapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bakalauras.certificatesapp.database.entities.CertificateEntity;

import java.util.List;

@Dao
public interface CertificateDao {
    @Query("SELECT * FROM certificateentity WHERE wallet_id=:walletId")
    LiveData<List<CertificateEntity>> getAllCertificatesByWalletID(String walletId);

    @Query("SELECT * FROM certificateentity WHERE certificate_name=:certificateName and wallet_id=:walletId LIMIT 1")
    LiveData<CertificateEntity> getCertificateByName(String certificateName, String walletId);

    @Insert
    void insert(CertificateEntity certificate);

    @Query("DELETE FROM certificateentity WHERE certificate_name=:certificateName AND wallet_id=:walletId")
    void delete(String walletId, String certificateName);

    @Update
    void update(CertificateEntity certificate);
}
