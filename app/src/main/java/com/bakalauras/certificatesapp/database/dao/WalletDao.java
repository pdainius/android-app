package com.bakalauras.certificatesapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bakalauras.certificatesapp.database.entities.WalletEntity;

import java.util.List;

@Dao
public interface WalletDao {
    @Query("SELECT * FROM walletentity")
    LiveData<List<WalletEntity>> getAllWallets();

    @Query("SELECT * FROM walletentity WHERE wallet_name=:walletName LIMIT 1")
    LiveData<WalletEntity> getWalletByName(String walletName);

    @Insert
    void insert(WalletEntity wallet);

    @Query("DELETE FROM walletentity WHERE wallet_name = :wallet_name")
    void delete(String wallet_name);

    @Update
    void update(WalletEntity wallet);
}