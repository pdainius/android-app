package com.bakalauras.certificatesapp.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.sql.Date;


@Entity
public class WalletEntity {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="wallet_name")
    private String walletName;

    @ColumnInfo(name="accessed_at")
    private Date accessedAt;

    @ColumnInfo(name="created_at")
    private Date createdAt;

    @ColumnInfo(name="note")
    private String note;

    @Ignore
    public WalletEntity() {}

    public WalletEntity(@NonNull String walletName, Date accessedAt, Date createdAt, String note) {
        this.walletName = walletName;
        this.accessedAt = accessedAt;
        this.createdAt = createdAt;
        this.note = note;
    }

    @NonNull
    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(@NonNull String walletName) {
        this.walletName = walletName;
    }

    public Date getAccessedAt() {
        return accessedAt;
    }

    public void setAccessedAt(Date accessedAt) {
        this.accessedAt = accessedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "WalletEntity{" +
                "walletName='" + walletName + '\'' +
                ", accessedAt=" + accessedAt +
                ", createdAt=" + createdAt +
                ", note='" + note + '\'' +
                '}';
    }
}
