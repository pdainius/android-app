package com.bakalauras.certificatesapp.activities;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.bakalauras.certificatesapp.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Random;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
@RunWith(AndroidJUnit4.class)
public class _2_CertificatesActivityTest {

    @Rule
    public ActivityTestRule<WalletDataActivity> mActivityTestRule = new ActivityTestRule<>(WalletDataActivity.class, false, false);

    @Rule
    public ActivityTestRule<CertificateDetailsActivity> mActivityTestRule2 = new ActivityTestRule<>(CertificateDetailsActivity.class, false, false);

    public static String certificateCommonName = null;
    private final String walletAlias = "test";

    @Before
    public void init(){
        if(certificateCommonName == null) {
            certificateCommonName = getRandomString();
        }
    }

    @Test
    public void _1_issueCertificateActivityTest() {
        Intent i = new Intent();
        i.putExtra("selectedWalletAlias", walletAlias);
        mActivityTestRule.launchActivity(i);

        ViewInteraction bottomNavigationItemView = onView(
                allOf(withId(R.id.wallet_bottom_navigation_certificates), withContentDescription("Certificates"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wallet_data_bottom_navigation),
                                        0),
                                1),
                        isDisplayed()));
        bottomNavigationItemView.perform(click());

        ViewInteraction floatingActionButton3 = onView(
                allOf(
                        withId(R.id.certificate_create_fab),
                        isDisplayed()
                )
        );
        floatingActionButton3.perform(click());

        ViewInteraction textInputEditText3 = onView(withId(R.id.common_name_input));
        textInputEditText3.perform(scrollTo(), replaceText(certificateCommonName), closeSoftKeyboard());

        ViewInteraction textInputEditText4 = onView(withId(R.id.country_input));
        textInputEditText4.perform(scrollTo(), replaceText("LT"), closeSoftKeyboard());

        ViewInteraction textInputEditText5 = onView(withId(R.id.state_input));
        textInputEditText5.perform(scrollTo(), replaceText("Lt"), closeSoftKeyboard());

        ViewInteraction textInputEditText6 = onView(withId(R.id.location_input));
        textInputEditText6.perform(scrollTo(), replaceText("Lt"), closeSoftKeyboard());

        ViewInteraction textInputEditText7 = onView(withId(R.id.organization_input));
        textInputEditText7.perform(scrollTo(), replaceText("Lt"), closeSoftKeyboard());

        ViewInteraction textInputEditText8 = onView(withId(R.id.organization_unit_input));
        textInputEditText8.perform(scrollTo(), replaceText("A"), closeSoftKeyboard());

        ViewInteraction materialButton = onView(withId(R.id.generate_csr_button));
        materialButton.perform(scrollTo(), click());

        ViewInteraction appCompatImageView = onView(
                allOf(
                        withId(R.id.copy_to_clipboard_icon),
                        isDisplayed()
                )
        );
        appCompatImageView.perform(scrollTo(), click());

        ViewInteraction materialButton3 = onView(withId(R.id.issue_crt_button));
        materialButton3.perform(scrollTo(), click());
    }

    @Test
    public void _2_certificateListTest() {
        Intent i = new Intent();
        i.putExtra("selectedWalletAlias", walletAlias);
        mActivityTestRule.launchActivity(i);

        ViewInteraction bottomNavigationItemView2 = onView(
                allOf(
                        withId(R.id.wallet_bottom_navigation_certificates),
                        isDisplayed()
                )
        );
        bottomNavigationItemView2.perform(click());

        DataInteraction relativeLayout2 = onData(anything())
                .inAdapterView(allOf(withId(R.id.certificates_list_view), isDisplayed(),
                        childAtPosition(
                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                0)))
                .atPosition(0);
        relativeLayout2.perform(click());

    }

    @Test
    public void _3_viewCertificateActivityTest() {
        Intent i = new Intent();
        i.putExtra("selectedWalletAlias", walletAlias);
        i.putExtra("selectedCertificateName", certificateCommonName);

        mActivityTestRule2.launchActivity(i);

        ViewInteraction appCompatImageView2 = onView(
                allOf(
                        withId(R.id.copy_to_clipboard_icon),
                        isClickable()
                )
        );
        appCompatImageView2.perform(scrollTo(), click());

        ViewInteraction materialButton4 = onView(
                allOf(
                        withId(R.id.certificate_delete_button),
                        isClickable()
                )
        );
        materialButton4.perform(scrollTo(), click());

        ViewInteraction materialButton5 = onView(allOf(withId(android.R.id.button3), isDisplayed()));
        materialButton5.perform(scrollTo(), click());
    }

    @Test
    public void _4_deleteCertificateActivityTest() {
        Intent i = new Intent();
        i.putExtra("selectedWalletAlias", walletAlias);
        i.putExtra("selectedCertificateName", certificateCommonName);

        mActivityTestRule2.launchActivity(i);

        ViewInteraction materialButton6 = onView(
                allOf(
                        withId(R.id.certificate_delete_button),
                        isClickable()
                )
        );
        materialButton6.perform(scrollTo(), click());

        ViewInteraction materialButton7 = onView(allOf(withId(android.R.id.button1), isClickable()));
        materialButton7.perform(scrollTo(), click());
    }

    protected String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
