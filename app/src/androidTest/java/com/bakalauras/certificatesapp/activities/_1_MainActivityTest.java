package com.bakalauras.certificatesapp.activities;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.bakalauras.certificatesapp.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Random;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class _1_MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        String walletName = "test";
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.wallet_menu_fab), withContentDescription("Button used to add/import wallets"),
                        childAtPosition(
                                allOf(withContentDescription("Button used to add/import wallets"),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                0),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.wallet_create_fab), withContentDescription("Button used to create and save generated wallet"),
                        childAtPosition(
                                allOf(withContentDescription("Button used to add/import wallets"),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                1),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction textInputEditText = onView(
                allOf(
                        withId(R.id.wallet_name_edit_text),
                        isDisplayed()
                )
        );
        textInputEditText.perform(replaceText(walletName), closeSoftKeyboard());

        ViewInteraction textInputEditText2 = onView(
                allOf(
                        withId(R.id.wallet_notes_input),
                        isDisplayed()
                )
        );
        textInputEditText2.perform(replaceText(walletName), closeSoftKeyboard());

        ViewInteraction materialButton = onView(
                allOf(
                        withId(R.id.wallet_create_button),
                        withText("Create Wallet"),
                        isDisplayed()
                )
        );
        materialButton.perform(click());

        DataInteraction relativeLayout = onData(anything())
                .inAdapterView(
                        withId(R.id.wallet_list_view)
                ).atPosition(0);
        relativeLayout.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}