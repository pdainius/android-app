package com.bakalauras.certificatesapp.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import com.bakalauras.certificatesapp.database.AppDatabase;
import com.bakalauras.certificatesapp.database.entities.CertificateEntity;
import com.bakalauras.certificatesapp.database.entities.WalletEntity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DaoTest {

    private CertificateDao certificateDao;
    private WalletDao walletDao;
    private AppDatabase database;

    private String walletName = "test_wallet";
    private Date date = new Date(System.currentTimeMillis());
    private String walletNote = "test_note";
    private String certificateName = "test_certificate";

    @Before
    public void init() {
        Context context = ApplicationProvider.getApplicationContext();
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        certificateDao = database.certificateDao();
        walletDao = database.walletDao();
    }

    @Test
    public void _1_insertTest() {
        WalletEntity walletEntity = new WalletEntity(walletName, date, date, walletNote);
        assertNotNull(walletEntity.toString());
        walletDao.insert(walletEntity);

        CertificateEntity certificateEntity = new CertificateEntity(walletName, certificateName, "test_msp", "x509",  date, date);
        assertNotNull(certificateEntity.toString());
        certificateDao.insert(certificateEntity);
    }

    @Test
    public void _2_deleteText() {
        WalletEntity walletEntity = new WalletEntity(walletName, date, date, walletNote);
        CertificateEntity certificateEntity = new CertificateEntity(walletName, certificateName, "test_msp", "x509",  date, date);
        walletDao.insert(walletEntity);
        certificateDao.insert(certificateEntity);

        certificateDao.delete(walletName, "test_cert_2");
    }

    @Test
    public void _2_updateTest() {
        WalletEntity walletEntity = new WalletEntity(walletName, date, date, walletNote);
        CertificateEntity certificateEntity = new CertificateEntity(walletName, certificateName, "test_msp", "x509",  date, date);
        walletDao.insert(walletEntity);
        certificateDao.insert(certificateEntity);
        walletEntity.setWalletName("test_wallet2");
        walletDao.update(walletEntity);

        assertEquals(walletEntity.getWalletName(), "test_wallet2");

        certificateEntity.setCertificateName("test_cert_2");
        certificateDao.update(certificateEntity);

        assertEquals(certificateEntity.getCertificateName(), "test_cert_2");
    }

    @After
    public void cleanup() {
        database.close();
    }
}
