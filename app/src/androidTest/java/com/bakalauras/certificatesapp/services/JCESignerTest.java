package com.bakalauras.certificatesapp.services;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.security.KeyPair;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
@SmallTest
public class JCESignerTest {
    private JCESigner jceSigner;

    @Before
    public void init() {
        KeyStoreService keyStoreService = KeyStoreService.getInstance();
        KeyPair keyPair = keyStoreService.generateSecretKey("test");
        jceSigner = new JCESigner(keyPair.getPrivate());
    }

    @Test
    public void _1_getSignatureTest() {
        byte[] signature = jceSigner.getSignature();
        assertNotNull(signature);
    }
}
