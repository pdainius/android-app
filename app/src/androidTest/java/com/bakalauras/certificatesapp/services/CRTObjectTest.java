package com.bakalauras.certificatesapp.services;

import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.X509v1CertificateBuilder;
import org.spongycastle.cert.jcajce.JcaX509CertificateConverter;
import org.spongycastle.operator.ContentSigner;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
@SmallTest
public class CRTObjectTest {

    private CRTObject crtObject;
    private String cn = "Test Test";

    @Before
    public void init() throws CertificateException {
        X509Certificate cert = generateSelfSignedCertificate();
        crtObject = new CRTObject(cert);
    }

    @Test
    public void _1_getSubjectStringTest() {
        String cnString = crtObject.getSubjectString(BCStyle.CN);
        assertEquals(cnString, cn);
    }

    @Test
    public void _2_getIssuerStringTest() {
        String cnString = crtObject.getIssuerString(BCStyle.CN);
        Log.d(cnString, cn);
    }

    @Test
    public void _3_toPemTest() {
        String pem = crtObject.toPem();
        assertNotNull(pem);
    }

    @Test
    public void _4_getKeyUsageUsagesTest() {
        String keyUsage = crtObject.getKeyUsageUsages();
        assertNull(keyUsage);
    }

    private X509Certificate generateSelfSignedCertificate() throws CertificateException {
        Date from = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
        Date to = new Date(System.currentTimeMillis() + 2 * 365 * 24 * 60 * 60 * 1000);

        KeyPair keyPair = KeyStoreService.getInstance().generateSecretKey("test");

        ContentSigner signer = new JCESigner(keyPair.getPrivate());

        X500Name name = new X500Name(String.format("CN=%s", cn));
        SubjectPublicKeyInfo subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded());
        X509v1CertificateBuilder certGen = new X509v1CertificateBuilder(name, BigInteger.valueOf(System.currentTimeMillis()), from, to, name, subPubKeyInfo);

        X509CertificateHolder certHolder = certGen.build(signer);
        return new JcaX509CertificateConverter().getCertificate(certHolder);
    }
}
