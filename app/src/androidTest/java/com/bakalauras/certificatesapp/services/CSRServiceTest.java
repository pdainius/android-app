package com.bakalauras.certificatesapp.services;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.io.IOException;
import java.security.KeyPair;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
@SmallTest
public class CSRServiceTest {
    private KeyStoreService keyStoreService;
    private CSRService csrService;
    private CSRObject csrObject;
    private String walletAlias;
    private KeyPair keyPair;

    @Before
    public void init() {
        csrService = CSRService.getInstance();
        keyStoreService = KeyStoreService.getInstance();
        walletAlias = "test_wallet_2";
        keyPair = keyStoreService.generateSecretKey(walletAlias);
        csrObject = new CSRObject(
                "test_cn",
                "LT",
                "Kaunas",
                "Kaunas",
                "KTU",
                "Student"
        );
    }

    @Test
    public void _2_generateCSRTest() throws IOException {
        assertNotNull(keyPair.getPublic());
        assertNotNull(keyPair.getPrivate().getAlgorithm());

        PKCS10CertificationRequest csr = csrService.generateCSR(keyPair, csrObject);
        assertNotNull(csr);
    }

    @Test
    public void _3_csrToPemTest() throws IOException {
        assertNotNull(keyPair.getPublic());
        assertNotNull(keyPair.getPrivate().getAlgorithm());
        PKCS10CertificationRequest csr = csrService.generateCSR(keyPair, csrObject);

        String pem = csrService.csrToPem(csr);
        assertNotNull(pem);
    }

    @Test
    public void _4_csrFromPemTest() throws IOException {
        assertNotNull(keyPair.getPublic());
        assertNotNull(keyPair.getPrivate().getAlgorithm());
        PKCS10CertificationRequest csr = csrService.generateCSR(keyPair, csrObject);
        String pem = csrService.csrToPem(csr);
        assertNotNull(pem);

        PKCS10CertificationRequest csr2 = csrService.csrFromPem(pem);
        assertEquals(csr, csr2);
    }

    @After
    public void cleanup() {
        keyStoreService.deleteAlias(walletAlias) ;
    }
}
