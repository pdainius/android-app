package com.bakalauras.certificatesapp.services;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.security.KeyPair;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
@SmallTest
public class KeyStoreServiceTest {
    private String keyPairName;
    private String certificateAlias;
    private KeyStoreService keyStoreService;
    private String crtPem;

    @Before
    public void init() {
        keyStoreService = KeyStoreService.getInstance();
        keyPairName = "test_key_pair";
        crtPem = "-----BEGIN CERTIFICATE-----\n" +
                "MIICjTCCAjOgAwIBAgIUYgK9ofUmpS6cJor21QI+LajPGIQwCgYIKoZIzj0EAwIw\n" +
                "WjELMAkGA1UEBhMCTFQxDzANBgNVBAgTBkthdW5hczEPMA0GA1UEBxMGS2F1bmFz\n" +
                "MQwwCgYDVQQKEwNLVFUxGzAZBgNVBAMTEmNhLmt0dS5kYWluaXVzLmRldjAeFw0y\n" +
                "MTA1MDMxMDQwMDBaFw0yMjA1MDMxMDQ1MDBaMFsxCzAJBgNVBAYTAkJWMQ0wCwYD\n" +
                "VQQIEwRWbnZiMQswCQYDVQQHEwJCdjEOMAwGA1UEChMFVmJiYmMxDzANBgNVBAsT\n" +
                "BmNsaWVudDEPMA0GA1UEAxMGS2Rodmp2MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcD\n" +
                "QgAEj5LP5cXh+GQy60o9xcFhwXTs07VGVMTZkONg4Nsuqw3uA3Ay42l9u6HZU7jD\n" +
                "pZCSnM9Se9s/OOX0Fvr9B+4OGqOB1TCB0jAOBgNVHQ8BAf8EBAMCB4AwDAYDVR0T\n" +
                "AQH/BAIwADAdBgNVHQ4EFgQUZaCc6HfStQWDZHjBWFDmBQoEkWAwHwYDVR0jBBgw\n" +
                "FoAU+VF1E6K+da+E09nmiAyfgWB93AYwcgYIKgMEBQYHCAEEZnsiYXR0cnMiOnsi\n" +
                "YmFrYWxhdXJhcy5pZCI6IktkaHZqdiIsImhmLkFmZmlsaWF0aW9uIjoiIiwiaGYu\n" +
                "RW5yb2xsbWVudElEIjoiS2Rodmp2IiwiaGYuVHlwZSI6ImNsaWVudCJ9fTAKBggq\n" +
                "hkjOPQQDAgNIADBFAiEA8wHVCR7tmwLnKp3tgnWkIVSltzkfokOwNmIc5O8wPY8C\n" +
                "IDNJUPSyjd1FIozvMHeuylpzUQjg/itvPTmNsoGuHCM2\n" +
                "-----END CERTIFICATE-----";
        certificateAlias = "cert_alias";
    }

    @Test
    public void _1_generateSecretKeyTest() {
        KeyPair keyPair = keyStoreService.generateSecretKey(keyPairName);
        assertNotNull(keyPair.getPublic());
        assertNotNull(keyPair.getPrivate().getAlgorithm());
    }

    @Test
    public void _2_getKeyPairTest() {
        KeyPair keyPair = keyStoreService.getKeyPair(keyPairName);
        assertNotNull(keyPair);
    }

    @Test
    public void _3_storeCertificateTest() {
        keyStoreService.storeCertificate(crtPem, certificateAlias);
        X509Certificate certificate = keyStoreService.getCertificate(certificateAlias);
        assertNotNull(certificate);
    }

    @Test
    public void _4_getCertificateTest() {
        X509Certificate certificate = keyStoreService.getCertificate(certificateAlias);
        assertNotNull(certificate);
    }

    @Test
    public void _4_getCertificateTest_2() {
        X509Certificate certificate = keyStoreService.getCertificate("nonExistingCertificate");
        assertNull(certificate);
    }

    @Test
    public void _5_deleteCertificateTest() {
        X509Certificate certificate = keyStoreService.getCertificate(certificateAlias);
        assertNotNull(certificate);

        keyStoreService.deleteCertificate(certificateAlias);
        X509Certificate certificate2 = keyStoreService.getCertificate(certificateAlias);
        assertNull(certificate2);
    }

    @Test
    public void _6_deleteAliasTest() {
        KeyPair keyPair = keyStoreService.getKeyPair(keyPairName);
        assertNotNull(keyPair);

        keyStoreService.deleteAlias(keyPairName);
        KeyPair keyPair2 = keyStoreService.getKeyPair(keyPairName);
        assertNull(keyPair2);
    }

    @Test
    public void _6_deleteAliasTest_2() {
        keyStoreService.deleteAlias("nonExistingWallet");
        KeyPair keyPair2 = keyStoreService.getKeyPair("nonExistingWallet");
        assertNull(keyPair2);
    }
}
